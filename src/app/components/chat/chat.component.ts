import { Component, OnInit } from "@angular/core";
import { Socket } from "ngx-socket-io";
import { ChatService, Message } from "src/app/services/chat.service";
import { getUnixTime } from "date-fns";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.css"],
})
export class ChatComponent implements OnInit {
  message = { content: "" };
  messages: Message[] = [];

  constructor(private chatService: ChatService) {
    this.messages = this.chatService.messages;
  }

  ngOnInit(): void {
    this.chatService.connect();
  }
  onMessageSend() {
    console.log(this.messages);
    const currentTime = new Date();
    this.chatService.sendMessage({
      ...this.message,
      timestamp: getUnixTime(currentTime),
    });
  }
}
