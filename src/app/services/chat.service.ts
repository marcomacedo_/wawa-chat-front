import { Injectable } from "@angular/core";
import { io, Socket } from "socket.io-client";
import { fromUnixTime, formatDistance } from "date-fns";

export type MessageData = { content: string; timestamp: number };
export type Message = MessageData & { from: "user" | "other"; date: string };

@Injectable({
  providedIn: "root",
})
export class ChatService {
  eventName = "chat-msg";
  socket: Socket;
  messages: Message[] = [];

  constructor() {}

  connect() {
    this.socket = io("localhost:8000");
    this.socket.on(this.eventName, (data) => this.getMessage(data));
  }

  sendMessage(data: MessageData) {
    const currentTime = new Date();
    const date = formatDistance(fromUnixTime(data.timestamp), currentTime);
    this.messages.push({ ...data, from: "user", date });
    this.socket.emit(this.eventName, data);
  }
  getMessage(data: MessageData) {
    const currentMessage = {
      ...data,
      date: this.getCurrentDateFormatted(data.timestamp),
      from: "other",
    };

    const messages = this.messages.map((m) => ({
      ...m,
      date: this.getCurrentDateFormatted(m.timestamp),
    }));
    console.log(messages);
    this.messages.push(currentMessage);
  }
  private getCurrentDateFormatted(timestamp: number) {
    const currentTime = new Date();
    const date = formatDistance(fromUnixTime(timestamp), currentTime);
    return date;
  }
}
